const User = require('../models/User');

exports.isValidExerciseData = function (data) {
    if (!data.duration) {
        throw new Error("Duration Cannot be left blank.");
    }

    if (!data.description) {
        throw new Error("Description Cannot be left blank.");
    }
}

exports.isValidISO8601Date = function (dateString) {
    /**
     * RegExp to test a string for a ISO 8601 Date spec
     *  YYYY
     *  YYYY-MM
     *  YYYY-MM-DD
     *  YYYY-MM-DDThh:mmTZD
     *  YYYY-MM-DDThh:mm:ssTZD
     *  YYYY-MM-DDThh:mm:ss.sTZD
     * @see: https://www.w3.org/TR/NOTE-datetime
     * @type {RegExp}
     */
    var ISO_8601 = /^\d{4}(-\d\d(-\d\d(T\d\d:\d\d(:\d\d)?(\.\d+)?(([+-]\d\d:\d\d)|Z)?)?)?)?$/i
    return ISO_8601.test(dateString);
}

exports.isValidDate = function (dateString) {
    return dateString && Helpers.isValidISO8601Date(dateString);
}

exports.removeSpaces = function (str) {
    if (!str) {
        return;
    }
    return str.replace(/\s+/g, '');
}

exports.getUserById = function (userId, req, res) {
    try {
        return User.findById(userId);
    } catch (error) {
        throw new Error({ message: 'Invalid UserId.' });
    }
}
