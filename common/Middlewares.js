const User = require('../models/User');
const Helpers = require('./Helpers');

exports.setUser = function(options) {
    return async (req, res, next) => {
        try {
            const userId = _getUserFromRequest(req, options);
            req.user = await User.findById(Helpers.removeSpaces(userId));
          } catch (error) {
            console.log('FetchUserError: ', JSON.stringify(error));
            return res.json({
              success: false,
              message: 'Invalid UserId.'
            });
          }
        next();
    }
}

exports.setFindQuery = function(req, res, next) {
    let userId  = req.params.userId;
    let findQuery = { 
        userId
      };
    let dateQuery = {};
  
    // from & to
    const fromDate = req.query.from || null;
    const toDate = req.query.to || null;
  
    // test if dates is in ISO8601 format
    if (fromDate && !Helpers.isValidISO8601Date(fromDate)) {
      return res.json({
        success: false,
        message: `The from date ${fromDate} is not a valid ISO8601 date.`
      });
    }

    if (toDate && !Helpers.isValidISO8601Date(toDate)) {
      return res.json({
        success: false,
        message: `The to date ${toDate} is not a valid ISO8601 date.`
      });
    } 

    if (fromDate) {
      dateQuery.date = { ...dateQuery.date, ...{ "$gte":  new Date(fromDate)} };
    }
    
    if (toDate) {
      dateQuery.date = { ...dateQuery.date, ...{ "$lte":  new Date(toDate)} };
    }

    // build and find query
    req.findQuery = (dateQuery.hasOwnProperty('date')) ? { ...findQuery, ...dateQuery }: findQuery;

    next();
}

function _getUserFromRequest(req, options) {
    if (options && options.isPost) {
        return req.body.userId;
    } else if (options && options.isQueryParam) {
        return req.query.userId;
    }

    return req.params.userId;
}
