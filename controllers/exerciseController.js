const Helpers = require('../common/Helpers');
const Exercise = require('../models/Exercise');


exports.create = async (req, res) => {
    let user = req.user;
    let {
      userId,
      description,
      duration,
      date
    } = req.body;
    userId = Helpers.removeSpaces(userId);

    // test if date is in ISO8601 format
    if (date && !Helpers.isValidISO8601Date(date)) {
      return res.json({
        success: false,
        message: `${date} is not a valid ISO8601 date.`
      });
    }
  
    const dateObj = (date) ? new Date(date) : new Date();
    const exercise = new Exercise({
      userId,
      description,
      duration,
      date: dateObj,
    });
  
    try {
      const newExercise = await exercise.save();
      res.json({ 
        userId: newExercise.userId,
        description: newExercise.description,
        duration: newExercise.duration,
        date: newExercise.date,
        username: user.username
      });
    } catch (error) {
      console.log('SaveExerciseError: ', error);
      return res.json({
        success: false,
        message: 'Invalid Exercise Data.',
        errorMessage: error.message || null
      });
    }
  }

  exports.findAllLogs = async (req, res) => {
    let user = req.user;
    let findQuery = req.findQuery;
    const limit = parseInt(req.query.limit, 10) || 0;
    try {
      const exercises = await Exercise.find(findQuery).limit(limit);
      const responseData = user._doc;
      responseData['log'] = exercises;
      responseData['count'] = exercises.length;
      res.json(responseData);
    } catch (error) {
      console.log('FetchExercisesError: ', JSON.stringify(error));
      return res.json({
        success: false,
        message: 'An error occured when fetching exercise log.'
      });
    }
  }