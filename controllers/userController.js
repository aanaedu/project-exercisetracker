const User = require('../models/User');

exports.create = async (req, res) => {
    const userName = req.body.username;
    try {
        const user = new User({
            username: userName
        });
        const savedUser = await user.save();
        res.send({
            username: savedUser.username,
            id: savedUser._id
        });
    } catch (error) {
        if (error.name === 'MongoError' && error.code === 11000) {
            // Duplicate username
            return res.status(500).send({
                success: false,
                message: 'User already exist!'
            });
        }

        // Some other error
        return res.status(500).send(error);
    }
}

exports.findAll = async (req, res) => {
    try {
        const users = await User.find({});
        res.json(users);
    } catch (error) {
        console.log('FetchUsersError: ', error);
        return res.status(500).send({
            success: false,
            message: 'An error occured while fetching users.'
        });
    }
}