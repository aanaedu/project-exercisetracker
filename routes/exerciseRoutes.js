const express = require('express');
const router = express.Router();

const exerciseController = require('../controllers/exerciseController');
const Middlewares = require('../common/Middlewares');

router.post('/add', Middlewares.setUser({
    isPost: true
}), exerciseController.create);
router.get('/log/:userId', [
    Middlewares.setUser(), Middlewares.setFindQuery
], exerciseController.findAllLogs);


module.exports = router;